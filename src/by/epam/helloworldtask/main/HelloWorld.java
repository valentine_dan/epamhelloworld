package by.epam.helloworldtask.main;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class prints Hello World to console.
 */
public final class HelloWorld {
    /**
     * Constructor.
     */
    private HelloWorld() { }
    /**
     * @param args - command line parameters
     */
    public static void main(final String[] args) {
        Logger log = Logger.getLogger(HelloWorld.class.getName());
        log.log(Level.INFO, "Hello World");
    }
}
